package cz.uhk.vojtele1.gameofflags.plugin.utils;

public enum AlgType {
    WIFI,
    BLE,
    COMBINED
}
