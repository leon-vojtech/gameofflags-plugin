package cz.uhk.vojtele1.gameofflags.plugin.model.firebase;

public class Player {

    private String nickname;
    private int experience;
    private int damagePoints;
    private int repairPoints;
    private int fraction;
    private boolean nameChanged;
    private int fractionChanged;
    private long lastChangeFractionAt;
    private int bonusFreePoints;
    private long lastRepairTimestamp;
    private long lastAttackTimestamp;

    public Player() {
        nickname = "nickname";
        fraction = 1;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getDamagePoints() {
        return damagePoints;
    }

    public void setDamagePoints(int damagePoints) {
        this.damagePoints = damagePoints;
    }

    public int getRepairPoints() {
        return repairPoints;
    }

    public void setRepairPoints(int repairPoints) {
        this.repairPoints = repairPoints;
    }

    public int getFraction() {
        return fraction;
    }

    public void setFraction(int fraction) {
        this.fraction = fraction;
    }

    public boolean isNameChanged() {
        return nameChanged;
    }

    public void setNameChanged(boolean nameChanged) {
        this.nameChanged = nameChanged;
    }

    public int getFractionChanged() {
        return fractionChanged;
    }

    public void setFractionChanged(int fractionChanged) {
        this.fractionChanged = fractionChanged;
    }

    public int getBonusFreePoints() {
        return bonusFreePoints;
    }

    public void setBonusFreePoints(int bonusFreePoints) {
        this.bonusFreePoints = bonusFreePoints;
    }

    public long getLastChangeFractionAt() {
        return lastChangeFractionAt;
    }

    public void setLastChangeFractionAt(long lastChangeFractionAt) {
        this.lastChangeFractionAt = lastChangeFractionAt;
    }

    public long getLastRepairTimestamp() {
        return lastRepairTimestamp;
    }

    public void setLastRepairTimestamp(long lastRepairTimestamp) {
        this.lastRepairTimestamp = lastRepairTimestamp;
    }

    public long getLastAttackTimestamp() {
        return lastAttackTimestamp;
    }

    public void setLastAttackTimestamp(long lastAttackTimestamp) {
        this.lastAttackTimestamp = lastAttackTimestamp;
    }
}
