package cz.uhk.vojtele1.gameofflags.plugin.model.firebase;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.vojtele1.gameofflags.plugin.model.BleScan;
import cz.uhk.vojtele1.gameofflags.plugin.model.WifiScan;

/**
 * Created by Matej Danicek on 7.11.2015.
 * Reworked by Leon Vojtech on 2016-2019
 */
public class Fingerprint {
    private List<WifiScan> wifiScans = new ArrayList<>();
    private List<BleScan> bleScans = new ArrayList<>();
    private int x;
    private int y;
    private String level = "";
    private long timestamp;

    // other recorded stuff...
    private float accX, accY, accZ, gyroX, gyroY, gyroZ, magX, magY, magZ;
    private String board, bootloader, brand, device, display, fingerprint, hardware, host, osId,
            manufacturer, model, product, serial, tags, type, user;
    private boolean supportsBLE;
    private String deviceID; // IMEI or MEID

    private String url;

    private double xCalc;
    private double yCalc;
    private String levelCalc;

    public Fingerprint() {
    }

    @Override
    public String toString() {
        return "Fingerprint{" +
                "wifiScans=" + wifiScans +
                ", bleScans=" + bleScans +
                ", timestamp=" + timestamp +
                ", accX=" + accX +
                ", accY=" + accY +
                ", accZ=" + accZ +
                ", gyroX=" + gyroX +
                ", gyroY=" + gyroY +
                ", gyroZ=" + gyroZ +
                ", magX=" + magX +
                ", magY=" + magY +
                ", magZ=" + magZ +
                ", board='" + board + '\'' +
                ", bootloader='" + bootloader + '\'' +
                ", brand='" + brand + '\'' +
                ", device='" + device + '\'' +
                ", display='" + display + '\'' +
                ", fingerprint='" + fingerprint + '\'' +
                ", hardware='" + hardware + '\'' +
                ", host='" + host + '\'' +
                ", osId='" + osId + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", product='" + product + '\'' +
                ", serial='" + serial + '\'' +
                ", tags='" + tags + '\'' +
                ", type='" + type + '\'' +
                ", user='" + user + '\'' +
                ", supportsBLE=" + supportsBLE +
                ", deviceID='" + deviceID + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", level='" + level + '\'' +
                ", xCalc=" + xCalc +
                ", yCalc=" + yCalc +
                ", levelCalc='" + levelCalc + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public List<WifiScan> getWifiScans() {
        return wifiScans;
    }

    public void setWifiScans(List<WifiScan> wifiScans) {
        this.wifiScans = wifiScans;
    }

    public List<BleScan> getBleScans() {
        return bleScans;
    }

    public void setBleScans(List<BleScan> bleScans) {
        this.bleScans = bleScans;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getAccX() {
        return accX;
    }

    public void setAccX(float accX) {
        this.accX = accX;
    }

    public float getAccY() {
        return accY;
    }

    public void setAccY(float accY) {
        this.accY = accY;
    }

    public float getAccZ() {
        return accZ;
    }

    public void setAccZ(float accZ) {
        this.accZ = accZ;
    }

    public float getGyroX() {
        return gyroX;
    }

    public void setGyroX(float gyroX) {
        this.gyroX = gyroX;
    }

    public float getGyroY() {
        return gyroY;
    }

    public void setGyroY(float gyroY) {
        this.gyroY = gyroY;
    }

    public float getGyroZ() {
        return gyroZ;
    }

    public void setGyroZ(float gyroZ) {
        this.gyroZ = gyroZ;
    }

    public float getMagX() {
        return magX;
    }

    public void setMagX(float magX) {
        this.magX = magX;
    }

    public float getMagY() {
        return magY;
    }

    public void setMagY(float magY) {
        this.magY = magY;
    }

    public float getMagZ() {
        return magZ;
    }

    public void setMagZ(float magZ) {
        this.magZ = magZ;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getBootloader() {
        return bootloader;
    }

    public void setBootloader(String bootloader) {
        this.bootloader = bootloader;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getOsId() {
        return osId;
    }

    public void setOsId(String osId) {
        this.osId = osId;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isSupportsBLE() {
        return supportsBLE;
    }

    public void setSupportsBLE(boolean supportsBLE) {
        this.supportsBLE = supportsBLE;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getxCalc() {
        return xCalc;
    }

    public void setxCalc(double xCalc) {
        this.xCalc = xCalc;
    }

    public double getyCalc() {
        return yCalc;
    }

    public void setyCalc(double yCalc) {
        this.yCalc = yCalc;
    }

    public String getLevelCalc() {
        return levelCalc;
    }

    public void setLevelCalc(String levelCalc) {
        this.levelCalc = levelCalc;
    }
}
