package cz.uhk.vojtele1.gameofflags.plugin.model;

import android.arch.persistence.room.Ignore;

/**
 * Created by Matej Danicek on 7.11.2015.
 * Reworked by Leon Vojtech on 2016-2019
 */
public class BleScan {

    private String address;
    private int rssi;
    private long time;

    public BleScan(String address, int rssi, long time) {
        this.address = address;
        this.rssi = rssi;
        this.time = time;
    }

    @Ignore
    public BleScan() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "{" +
                "address='" + address + '\'' +
                ", rssi=" + rssi +
                ", time=" + time +
                '}';
    }
}
