package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.support.v7.app.AppCompatActivity;

import cz.uhk.vojtele1.gameofflags.plugin.service.impl.AuthenticationServiceImpl;
import cz.uhk.vojtele1.gameofflags.plugin.service.impl.CaptureServiceImpl;
import cz.uhk.vojtele1.gameofflags.plugin.service.impl.ErrorServiceImpl;
import cz.uhk.vojtele1.gameofflags.plugin.service.impl.FirebaseDatabaseServiceImpl;
import cz.uhk.vojtele1.gameofflags.plugin.service.impl.LocalizationServiceImpl;
import cz.uhk.vojtele1.gameofflags.plugin.service.impl.SharedPreferencesServiceImpl;

public abstract class InstanceService {

    /**
     * create instances for all services, that need activity
     *
     * @param activity that is running (unityActivity)
     */
    public static void createServicesInstances(AppCompatActivity activity) {
        SharedPreferencesServiceImpl.getInstance().setActivity(activity);
        ErrorServiceImpl.getInstance().setActivity(activity); // for toasts
        CaptureServiceImpl.getInstance().setActivity(activity); // for sensors and device informations
        AuthenticationServiceImpl.getInstance().setActivity(activity);
        LocalizationServiceImpl.getInstance().setActivity(activity);
        FirebaseDatabaseServiceImpl.getInstance().setActivity(activity); // for insert new scans for localization to local db
    }

    /**
     * If you need change db, do it here
     *
     * @return database instance
     */
    public static DatabaseService getDatabaseInstance() {
        return FirebaseDatabaseServiceImpl.getInstance();
    }

    public static ErrorService getErrorInstance() {
        return ErrorServiceImpl.getInstance();
    }

    public static CaptureService getCaptureInstance() {
        return CaptureServiceImpl.getInstance();
    }

    // It is used in Unity
    public static AuthenticationService getAuthenticationInstance() {
        return AuthenticationServiceImpl.getInstance();
    }

    public static LocalizationService getLocalizationInstance() {
        return LocalizationServiceImpl.getInstance();
    }

    public static SharedPreferencesService getSharedPreferencesInstance() {
        return SharedPreferencesServiceImpl.getInstance();
    }
}
