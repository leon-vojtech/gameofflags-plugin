package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.app.Activity;

import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Fingerprint;

public interface CaptureService {

    void setActivity(Activity activity);

    void createFingerprint();

    Fingerprint getFingerprint();

    // save experience and new tower state (maybe captured) to db, called from unity
    void attackRepairAction(String action, int dmgRepair, String towerName);
}
