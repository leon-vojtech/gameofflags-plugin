package cz.uhk.vojtele1.gameofflags.plugin.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import cz.uhk.vojtele1.gameofflags.plugin.dao.ScanDao;
import cz.uhk.vojtele1.gameofflags.plugin.database.AppDatabase;
import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;

import java.util.List;

public class AppRepository {

    private ScanDao scanDao;
    private static LiveData<List<Scan>> scans;

    public AppRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        scanDao = db.scanDao();
        scans = scanDao.getAllLive();
    }

    public LiveData<List<Scan>> getAllScan() {
        return scans;
    }

    public void insertScan(Scan scan) {
        new insertAsyncTaskScan(scanDao).execute(scan);
    }

    public void deleteAllOwnScans() {
        new deleteAsyncTaskScans(scanDao).execute();
    }

    private static class insertAsyncTaskScan extends AsyncTask<Scan, Void, Void> {

        private ScanDao scanDao;

        insertAsyncTaskScan(ScanDao dao) {
            scanDao = dao;
        }

        @Override
        protected Void doInBackground(final Scan... params) {
            scanDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTaskScans extends AsyncTask<Void, Void, Void> {

        private final ScanDao scanDao;

        deleteAsyncTaskScans(ScanDao dao) {
            scanDao = dao;
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            scanDao.deleteAll();
            return null;
        }
    }
}