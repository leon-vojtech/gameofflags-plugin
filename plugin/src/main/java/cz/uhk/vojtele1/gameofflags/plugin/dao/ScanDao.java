package cz.uhk.vojtele1.gameofflags.plugin.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;

import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;

import java.util.List;

@Dao
public interface ScanDao {
    @Query("SELECT * FROM Scan")
    LiveData<List<Scan>> getAllLive();

    @Query("SELECT COUNT(*) FROM Scan")
    int getSize();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Scan> scans);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Scan bleScan);

    @Query("DELETE FROM Scan")
    void deleteAll();
}
