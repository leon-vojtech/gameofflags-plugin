package cz.uhk.vojtele1.gameofflags.plugin.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;
import cz.uhk.vojtele1.gameofflags.plugin.repository.AppRepository;

import java.util.List;

public class AppViewModel extends AndroidViewModel {

    private AppRepository repository;

    private LiveData<List<Scan>> scans;

    public AppViewModel (Application application) {
        super(application);
        repository = new AppRepository(application);
        scans = repository.getAllScan();
    }

    public LiveData<List<Scan>> getAllScans() {
        return scans;
    }

    public void insertScan(Scan scan) { repository.insertScan(scan); }

    public void deleteAllOwnScans() {
        repository.deleteAllOwnScans();
    }
}