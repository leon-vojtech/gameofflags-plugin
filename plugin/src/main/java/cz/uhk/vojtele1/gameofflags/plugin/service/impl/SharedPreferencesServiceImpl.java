package cz.uhk.vojtele1.gameofflags.plugin.service.impl;

import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import cz.uhk.vojtele1.gameofflags.plugin.service.SharedPreferencesService;
import cz.uhk.vojtele1.gameofflags.plugin.utils.C;

public class SharedPreferencesServiceImpl implements SharedPreferencesService {

    private static SharedPreferencesServiceImpl instance;
    private SharedPreferences prefs;

    public static SharedPreferencesServiceImpl getInstance() {
        if (instance == null) {
            instance = new SharedPreferencesServiceImpl();
        }
        return instance;
    }

    private SharedPreferencesServiceImpl() {
    }

    public void setActivity(AppCompatActivity activity) {
        prefs = activity.getSharedPreferences(C.SHARED_PREFERENCES, MODE_PRIVATE);
    }

    public String getFloor() {
        return prefs.getString(C.PLAYER_FLOOR, C.DEFAULT_FLOOR);
    }

    public void setFloor(String floor) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(C.PLAYER_FLOOR, floor);
        editor.apply();
    }
}
