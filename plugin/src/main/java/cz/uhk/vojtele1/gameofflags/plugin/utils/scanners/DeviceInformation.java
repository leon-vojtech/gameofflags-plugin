package cz.uhk.vojtele1.gameofflags.plugin.utils.scanners;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Fingerprint;

/**
 * Trida ziskavajici informace o zarizeni
 * created by Dominik Matoulek 2015
 * edited by Leon Vojtech 2019
 */
public class DeviceInformation {

    Activity activity;

    /**
     * Inicializuje DeviceInformation
     * @param activity activity
     */
    public DeviceInformation(Activity activity) {
        this.activity = activity;
    }

    /**
     * Naplni fingerprint daty o zarizeni, ktere fingerprint delalo
     * @param p fingerprint vhodny k naplneni daty
     * @return fingerprint s naplnenymi daty
     */
    public Fingerprint fillPosition(Fingerprint p) {
        p.setSupportsBLE(activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE));
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (null != ((TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE)).getImei())
                    p.setDeviceID(((TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE)).getImei());
                else
                    p.setDeviceID(((TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE)).getMeid());
            } else {
                p.setDeviceID(((TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
            }
        } else {
            // dont handle request code, it will not wait for permissions, so it could be anything
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        }
        p.setBoard(Build.BOARD);
        p.setBootloader(Build.BOOTLOADER);
        p.setBrand(Build.BRAND);
        p.setDevice(Build.DEVICE);
        p.setDisplay(Build.DISPLAY);
        p.setFingerprint(Build.FINGERPRINT);
        p.setHardware(Build.HARDWARE);
        p.setHost(Build.HOST);
        p.setOsId(Build.ID);
        p.setManufacturer(Build.MANUFACTURER);
        p.setModel(Build.MODEL);
        p.setProduct(Build.PRODUCT);
        p.setSerial(Build.SERIAL);
        p.setTags(Build.TAGS);
        p.setType(Build.TYPE);
        p.setUser(Build.USER);
        return p;
    }
}