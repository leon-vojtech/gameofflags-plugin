package cz.uhk.vojtele1.gameofflags.plugin.service.impl;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.uhk.vojtele1.gameofflags.plugin.NN.NearestNeighbor;
import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Fingerprint;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.OnlinePlayer;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Player;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Position;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Tower;
import cz.uhk.vojtele1.gameofflags.plugin.service.DatabaseService;
import cz.uhk.vojtele1.gameofflags.plugin.service.ErrorService;
import cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService;
import cz.uhk.vojtele1.gameofflags.plugin.utils.C;
import cz.uhk.vojtele1.gameofflags.plugin.utils.Utils;
import cz.uhk.vojtele1.gameofflags.plugin.viewModel.AppViewModel;

/**
 * Every method is used, some of them are called only from unity
 */
public class FirebaseDatabaseServiceImpl implements DatabaseService {

    private static final String ONLINE_PLAYERS = "onlinePlayers";
    private static final String SCANS = "scans";
    private static final String PLAYERS = "players";
    private static final String FINGERPRINTS = "fingerprints";
    private static final String TOWERS = "towers";
    private static DatabaseService instance;

    private FirebaseFirestore dbFirestore;
    private DatabaseReference dbRealtime;
    private StorageReference storage;
    private Player player;
    private ListenerRegistration playerListener;
    private ErrorService errorService;
    private ChildEventListener towersEventListener;
    private Map<String, Tower> towers = new HashMap<>();

    /**
     * need to know for pause / unpause, because remove() on listener returns void
     */
    private boolean isPlayerListenerRegistered;
    private boolean isPlayerOnlineListenerRegistered;
    private boolean isTowersListenerRegistered;

    private boolean isApplicationResume;
    private boolean isTowerDataObtained;

    private boolean isPlayerTowerDataSaved;
    private boolean isPlayerFractionChanged = true;

    private ChildEventListener onlinePlayersEventListener;
    private Map<String, OnlinePlayer> onlinePlayers = new HashMap<>();

    private AppViewModel appViewModel;

    public static DatabaseService getInstance() {
        if (instance == null) {
            instance = new FirebaseDatabaseServiceImpl();
        }
        return instance;
    }

    private FirebaseDatabaseServiceImpl() {
        storage = FirebaseStorage.getInstance().getReference();
        dbFirestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        dbFirestore.setFirestoreSettings(settings);
        FirebaseDatabase.getInstance().setPersistenceEnabled(false);
        dbRealtime = FirebaseDatabase.getInstance().getReference();
        errorService = InstanceService.getErrorInstance();
        initTowersEventListener();
        initOnlinePlayersEventListener();
    }

    public void setActivity(AppCompatActivity activity) {
        appViewModel = ViewModelProviders.of(activity).get(AppViewModel.class);
    }

    public void addPlayerListener() {
        final String userUid = getUserUid();
        if (userUid != null) {
            if (!isPlayerListenerRegistered) {
                Log.d(C.TAG, "Registering player listener.");
                DocumentReference docRef = dbFirestore.collection(PLAYERS).document(userUid);
                playerListener = docRef.addSnapshotListener((document, e) -> {
                    if (e != null) {
                        Log.w(C.TAG, "Listen failed.", e);
                        errorService.showToast("Nepodařilo se získat data, obraťte se na administrátora.");
                        return;
                    }
                    if (document != null && document.exists()) {
                        Log.d(C.TAG, "Player: " + document.getData());
                        player = document.toObject(Player.class);
                    } else if (document != null && document.getMetadata().isFromCache()) {
                        Log.d(C.TAG, "Player not obtained. Probably not connected to internet.");
                    } else {
                        Log.d(C.TAG, "Player does not exists. Creating new.");
                        docRef.set(new Player());
                    }
                });
                isPlayerListenerRegistered = true;
            }
        } else {
            player = null;
            Log.d(C.TAG, "User is not logged.");
        }
    }

    public void removePlayerListener() {
        if (playerListener != null) {
            playerListener.remove();
            isPlayerListenerRegistered = false;
            Log.d(C.TAG, "PlayerListener removed.");
        }
    }

    // called from PlayerController, could be null
    public Player getPlayer() {
        //    Log.d(C.TAG, "Called getPlayer. Player is: " + player);
        return player;
    }

    public void setPlayer(String nickname, int fraction) {
        final String userUid = getUserUid();
        if (userUid != null) {
            DocumentReference docRef = dbFirestore.collection(PLAYERS).document(userUid);
            docRef.update("nickname", nickname,
                    "fraction", fraction,
                    "nameChanged", true,
                    "fractionChanged", player.getFractionChanged() + 1);
        }
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getUserUid() {
        // Log.d(C.TAG, "Getting user uid.");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            //   Log.d(C.TAG, "User uid: " + user.getUid());
            return user.getUid();
        }
        // Log.d(C.TAG, "User is not logged.");
        return null;
    }

    public void addFingerprint(Fingerprint fingerprint) {
        dbFirestore.collection(FINGERPRINTS).document(System.currentTimeMillis() + "_" + fingerprint.getLevel()
                + "_" + fingerprint.getX() + "_" + fingerprint.getY()).set(fingerprint);
        Log.d(C.TAG, "Adding fingerprint.");
    }

    public void addDamage() {
        final String userUid = getUserUid();
        if (userUid != null) {
            DocumentReference docRef = dbFirestore.collection(PLAYERS).document(userUid);
            docRef.update("damagePoints", player.getDamagePoints() + 1);
            Log.d(C.TAG, "Increasing damage.");
        }
    }

    public void addRepair() {
        final String userUid = getUserUid();
        if (userUid != null) {
            DocumentReference docRef = dbFirestore.collection(PLAYERS).document(userUid);
            docRef.update("repairPoints", player.getRepairPoints() + 1);
            Log.d(C.TAG, "Increasing repair.");
        }
    }

    public Map<String, Tower> getTowersByFloor(String floor) {
        Log.d(C.TAG, "Returning towers for floor: " + floor);
        Map<String, Tower> towersMap = new HashMap<>();
        for (Map.Entry<String, Tower> entry : towers.entrySet()) {
            if (floor.matches(entry.getValue().getFloor())) {
                towersMap.put(entry.getKey(), entry.getValue());
            }
        }
        Log.d(C.TAG, "Returned towers: " + towersMap.keySet());

        return towersMap;
    }

    public int getTowersCountForFraction(int fractionId) {
        int countedTowers = 0;
        for (Tower tower : towers.values()) {
            if (tower.getFraction() == fractionId) {
                countedTowers++;
            }
        }

        return countedTowers;
    }

    public void registerListeners() {
        isApplicationResume = true;
        isTowerDataObtained = false;
        addPlayerListener();
        addTowersListener();
        addPlayerOnlineListener();
    }

    public void removeListeners() {
        isApplicationResume = false;
        isTowerDataObtained = true;
        removePlayerListener();
        removeTowersListener();
        removePlayerOnlineListener();
        player = null;
    }

    public void addTowersListener() {
        Log.d(C.TAG, "Registering towers listener.");
        if (!isTowersListenerRegistered) {
            dbRealtime.child(TOWERS).addChildEventListener(towersEventListener);
            isTowersListenerRegistered = true;
        }
    }

    public void removeTowersListener() {
        Log.d(C.TAG, "TowersListener removed.");
        if (isTowersListenerRegistered) {
            dbRealtime.child(TOWERS).removeEventListener(towersEventListener);
            isTowersListenerRegistered = false;
        }
    }

    private void initTowersEventListener() {
        towersEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(C.TAG, "TowerListener-onChildAdded:" + dataSnapshot.getKey());
                towers.put(dataSnapshot.getKey(), dataSnapshot.getValue(Tower.class));
                isTowerDataObtained = true;
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(C.TAG, "TowerListener-onChildChanged:" + dataSnapshot.getKey());
                towers.put(dataSnapshot.getKey(), dataSnapshot.getValue(Tower.class));
                isTowerDataObtained = true;
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(C.TAG, "TowerListener-onChildRemoved:" + dataSnapshot.getKey());
                towers.remove(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(C.TAG, "TowerListener-onChildMoved:" + dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(C.TAG, "TowerListener-onCancelled:", databaseError.toException());
            }
        };
    }

    public boolean databaseActual() {
        Log.w("debug", "databaseActual-player null: " + (player == null) + " towers obtained: " + isTowerDataObtained);
        if (getUserUid() != null) {
            return player != null && isTowerDataObtained;
        }

        return isTowerDataObtained;
    }

    public boolean isApplicationResume() {
        return isApplicationResume;
    }

    public void setApplicationResume(boolean applicationResume) {
        isApplicationResume = applicationResume;
    }

    public void changeFraction() {
        final String userUid = getUserUid();
        if (userUid != null && player != null && player.getLastChangeFractionAt() + C.WEEK_MILLISECONDS < System.currentTimeMillis()) {
            isPlayerFractionChanged = false;
            int fraction = player.getFraction() == 2 ? 1 : 2;
            DocumentReference docRef = dbFirestore.collection(PLAYERS).document(userUid);
            docRef.update(
                    "fraction", fraction,
                    "fractionChanged", player.getFractionChanged() + 1,
                    "lastChangeFractionAt", System.currentTimeMillis())
                    .addOnSuccessListener(success -> {
                        Log.d(C.TAG, "Successfully changed player (ar).");
                        isPlayerFractionChanged = true;
                    });
        }
    }

    public boolean isPlayerFractionChanged() {
        return isPlayerFractionChanged;
    }

    // https://firebase.google.com/docs/storage/android/upload-files#get_a_download_url
    public void saveFingerprintWithScreenshot(byte[] bytes, String floor, int x, int y) {
        StorageReference screenShotRef = storage.child("fingerprintScreenShot/" + System.currentTimeMillis() + "_" + floor + "_" + x + "_" + y + ".jpg");
        UploadTask uploadTask = screenShotRef.putBytes(bytes);

        uploadTask.continueWithTask(task -> {
            // Continue with the task to get the download URL
            return screenShotRef.getDownloadUrl();
        }).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String url = task.getResult().toString();
                final Fingerprint fingerprint = InstanceService.getCaptureInstance().getFingerprint();
                fingerprint.setUrl(url);
                fingerprint.setLevel(floor);
                fingerprint.setX(x);
                fingerprint.setY(y);
                final NearestNeighbor calculatedPosition = InstanceService.getLocalizationInstance().getMyPosition();
                fingerprint.setxCalc(calculatedPosition.getX());
                fingerprint.setyCalc(calculatedPosition.getY());
                fingerprint.setLevelCalc(InstanceService.getSharedPreferencesInstance().getFloor());

                addFingerprint(fingerprint);
            }
        });
    }

    public void saveAttackRepairTower(String action, int dmgRepair, String towerName) {
        Log.d(C.TAG, "Updating database after ar action.");
        Tower tower = towers.get(towerName);
        final String userUid = getUserUid();
        if (tower == null || userUid == null) { // should not happen
            return;
        }
        int maxHP = Utils.calculateTowerMaxHealthPoints(Utils.calculateLevel(tower.getPopularity()));
        int playerExperience = player.getExperience();
        long lastRepairTimestamp = player.getLastRepairTimestamp();
        long lastAttackTimestamp = player.getLastAttackTimestamp();
        long currentTimestamp = System.currentTimeMillis();
        if (action.matches(C.AR_ATTACK)) {
            // if tower is captured
            if (tower.getHealthPoints() - dmgRepair <= 0) {
                tower.setHealthPoints(maxHP / 2);
                tower.setFraction(tower.getFraction() == 1 ? 2 : 1);
                tower.setCapturedAt(currentTimestamp);
                tower.setCapturedBy(player.getNickname());

                playerExperience += C.ATTACK_REPAIR_EXPERIENCE_INCREASE; // added more exp if capture
            } else {
                tower.setHealthPoints(tower.getHealthPoints() - dmgRepair);
            }
            lastAttackTimestamp = currentTimestamp;
        } else if (action.matches(C.AR_REPAIR)) {
            tower.setHealthPoints(tower.getHealthPoints() + dmgRepair);
            if (tower.getHealthPoints() > maxHP) { // should not happen - rounded in unity
                tower.setHealthPoints(maxHP);
            }
            lastRepairTimestamp = currentTimestamp;
        }
        tower.setPopularity(tower.getPopularity() + C.TOWER_POPULARITY_INCREASE);
        playerExperience += C.ATTACK_REPAIR_EXPERIENCE_INCREASE;

        DocumentReference docRef = dbFirestore.collection(PLAYERS).document(userUid);
        isPlayerTowerDataSaved = false;
        final int finalPlayerExperience = playerExperience;
        final long finalLastRepairTimestamp = lastRepairTimestamp;
        final long finalLastAttackTimestamp = lastAttackTimestamp;

        // this should be transaction
        dbRealtime.child(TOWERS).child(towerName).setValue(tower).addOnSuccessListener(
                (successRt) -> {
                    Log.d(C.TAG, "Successfully changed tower (ar).");
                    docRef.update("experience", finalPlayerExperience, "lastAttackTimestamp", finalLastAttackTimestamp,
                            "lastRepairTimestamp", finalLastRepairTimestamp).addOnSuccessListener(
                            successFs -> {
                                Log.d(C.TAG, "Successfully changed player (ar).");
                                isPlayerTowerDataSaved = true;
                            }).addOnFailureListener(fail -> Log.d(C.TAG, "Failure in changing player (ar)."));
                }).addOnFailureListener(fail -> Log.d(C.TAG, "Failure in changing tower (ar)."));

        docRef.update("experience", playerExperience);
    }

    public boolean isPlayerTowerDataSaved() {
        return isPlayerTowerDataSaved;
    }

    public void setPlayerOnline(Player player) {
        final String userUid = getUserUid();
        if (userUid != null && player != null) {
            NearestNeighbor myPosition = InstanceService.getLocalizationInstance().getMyPosition();
            Position position = new Position((int) myPosition.getX(), (int) myPosition.getY(), myPosition.getLevel());
            OnlinePlayer onlinePlayer = new OnlinePlayer(player.getNickname(), player.getFraction(), player.getExperience(), System.currentTimeMillis(), position);
            dbRealtime.child(ONLINE_PLAYERS).child(userUid).setValue(onlinePlayer);
        }
    }

    public void addPlayerOnlineListener() {
        Log.d(C.TAG, "Registering PlayerOnlineListener.");
        if (!isPlayerOnlineListenerRegistered) {
            dbRealtime.child(ONLINE_PLAYERS).addChildEventListener(onlinePlayersEventListener);
            isPlayerOnlineListenerRegistered = true;
        }
    }

    public void removePlayerOnlineListener() {
        Log.d(C.TAG, "PlayerOnlineListener removed.");
        if (isPlayerOnlineListenerRegistered) {
            dbRealtime.child(ONLINE_PLAYERS).removeEventListener(onlinePlayersEventListener);
            isPlayerOnlineListenerRegistered = false;
        }
    }

    private void initOnlinePlayersEventListener() {
        onlinePlayersEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(C.TAG, "OnlinePlayerListener-onChildAdded:" + dataSnapshot.getKey());
                onlinePlayers.put(dataSnapshot.getKey(), dataSnapshot.getValue(OnlinePlayer.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(C.TAG, "OnlinePlayerListener-onChildChanged:" + dataSnapshot.getKey());
                onlinePlayers.put(dataSnapshot.getKey(), dataSnapshot.getValue(OnlinePlayer.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(C.TAG, "OnlinePlayerListener-onChildRemoved:" + dataSnapshot.getKey());
                onlinePlayers.remove(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(C.TAG, "OnlinePlayerListener-onChildMoved:" + dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(C.TAG, "OnlinePlayerListener-onCancelled:", databaseError.toException());
            }
        };
    }

    public Map<String, OnlinePlayer> getOnlinePlayersByFloor(String floor) {
        Map<String, OnlinePlayer> playersOnline = new HashMap<>();

        Map<String, OnlinePlayer> onlinePlayersCopy = new HashMap<>(onlinePlayers);
        // remove myself
        final String userUid = getUserUid();
        if (userUid != null) {
            onlinePlayersCopy.remove(userUid);
        }
        for (Map.Entry<String, OnlinePlayer> playerOnlineEntry : onlinePlayersCopy.entrySet()) {
            if (playerOnlineEntry.getValue().getLastChangedAt() + C.ONLINE_PLAYERS_MAX_INACTIVE_TIME > System.currentTimeMillis()
                    && playerOnlineEntry.getValue().getPosition().getFloor().matches(floor)) {
                playersOnline.put(playerOnlineEntry.getKey(), playerOnlineEntry.getValue());
            }
        }

        List<String> onlinePlayersName = new ArrayList<>();
        for (OnlinePlayer onlinePlayer : playersOnline.values()) {
            onlinePlayersName.add(onlinePlayer.getUsername());
        }
        Log.d(C.TAG, "Returned online players: " + onlinePlayersName);

        return playersOnline;
    }

    public void getNewScans(long lastScanTimestamp) {
        // start after lastScanTimestamp
        dbRealtime.child(SCANS).orderByChild("timestamp").startAt(lastScanTimestamp + 1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(C.TAG, "New scans count: " + dataSnapshot.getChildrenCount());
                List<Scan> scans = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Scan scan = child.getValue(Scan.class);
                    scans.add(scan);
                }
                insertNewScansToLocalDb(scans);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(C.TAG, databaseError.getMessage());
            }
        });
    }

    /**
     * insert scans to local database
     *
     * @param scans - new scans that will be inserted
     */
    private void insertNewScansToLocalDb(List<Scan> scans) {
        for (Scan scan : scans) {
            appViewModel.insertScan(scan);
        }
    }
}
