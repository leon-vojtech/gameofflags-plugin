package cz.uhk.vojtele1.gameofflags.plugin.service.impl;

import android.app.Activity;
import android.widget.Toast;

import cz.uhk.vojtele1.gameofflags.plugin.service.ErrorService;

public class ErrorServiceImpl implements ErrorService {

    private static ErrorServiceImpl instance;

    private Activity activity;
    private Toast toast;

    public static ErrorServiceImpl getInstance() {
        if (instance == null) {
            instance = new ErrorServiceImpl();
        }
        return instance;
    }

    private ErrorServiceImpl() {
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void showToast(String text) {
        if (toast == null)
            toast = Toast.makeText(activity, text, Toast.LENGTH_LONG);

        if (!toast.getView().isShown()) {
            toast.setText(text);
            toast.show();
        }
    }

    public void errorLoadingTooLong() {
        showToast("Něco se pokazilo, zkus to prosím později.");
    }
}
