package cz.uhk.vojtele1.gameofflags.plugin.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import cz.uhk.vojtele1.gameofflags.plugin.dao.ScanDao;
import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@Database(entities = {Scan.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ScanDao scanDao();

    private static AppDatabase INSTANCE;

    private static Context context;

    public static AppDatabase getDatabase(final Context cont) {
        context = cont;
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "appDatabase")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()

                            // clear db
                             //  .addCallback(roomDatabaseCallback)
                            .addCallback(populateDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback roomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new ClearDb(INSTANCE).execute();
                }
            };

    private static RoomDatabase.Callback populateDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDb(INSTANCE).execute();
                }
            };

    private static class ClearDb extends AsyncTask<Void, Void, Void> {

        private final ScanDao scanDao;

        ClearDb(AppDatabase db) {
            scanDao = db.scanDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            scanDao.deleteAll();
            return null;
        }
    }

    private static class PopulateDb extends AsyncTask<Void, Void, Void> {

        private final ScanDao scanDao;

        PopulateDb(AppDatabase db) {
            scanDao = db.scanDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            if (scanDao.getSize() == 0) {
                System.out.println("empty db, filling");
                Gson gson = new Gson();
                AssetManager assetManager = context.getApplicationContext().getAssets();
                try {
                    Reader reader;
                    for (int i = 1; i <= 4; i++) {
                        reader = new InputStreamReader(assetManager.open("databasej" + i + ".json"));
                        scanDao.insertAll(gson.fromJson(reader, new TypeToken<List<Scan>>(){}.getType()));
                        reader.close();
                        System.out.println("Size of db: " + scanDao.getSize());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
