package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.app.Activity;

public interface AuthenticationService {

    void setActivity(Activity activity);

    void createSignInIntent();

    void signOut();
}
