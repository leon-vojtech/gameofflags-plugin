package cz.uhk.vojtele1.gameofflags.plugin.service.impl;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.uhk.vojtele1.gameofflags.plugin.NN.CalculatePosition;
import cz.uhk.vojtele1.gameofflags.plugin.NN.CalculateSignalDistance;
import cz.uhk.vojtele1.gameofflags.plugin.NN.NearestNeighbor;
import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;
import cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService;
import cz.uhk.vojtele1.gameofflags.plugin.service.LocalizationService;
import cz.uhk.vojtele1.gameofflags.plugin.utils.AlgType;
import cz.uhk.vojtele1.gameofflags.plugin.utils.C;
import cz.uhk.vojtele1.gameofflags.plugin.viewModel.AppViewModel;

public class LocalizationServiceImpl implements LocalizationService {

    private static LocalizationService instance;

    private NearestNeighbor myPosition;
    private CalculatePosition cp;
    private List<Scan> scans = new ArrayList<>();
    private Scan newScan = null;
    private int k = 2;
    private boolean isCalculatedNewPosition;
    private boolean isCalledGetNewScans;

    public static LocalizationService getInstance() {
        if (instance == null) {
            instance = new LocalizationServiceImpl();
        }
        return instance;
    }

    private LocalizationServiceImpl() {
        CalculateSignalDistance csd = new CalculateSignalDistance();
        cp = new CalculatePosition(csd);
        myPosition = new NearestNeighbor(0, 0, InstanceService.getSharedPreferencesInstance().getFloor(), 0);
    }

    public void setActivity(AppCompatActivity activity) {
        AppViewModel appViewModel = ViewModelProviders.of(activity).get(AppViewModel.class);
        appViewModel.getAllScans().observe(activity, scansFromLive -> {
            if (scansFromLive != null) {
                Log.d(C.TAG, "Local database scans: " + scansFromLive.size());
                scans = new ArrayList<>(scansFromLive);
                long lastTimestamp = 0;
                for (Scan scan : scansFromLive) {
                    if (lastTimestamp < scan.getTimestamp()) {
                        lastTimestamp = scan.getTimestamp();
                    }
                }
                if (!isCalledGetNewScans) {
                    Log.d(C.TAG, "LastScanTimestamp: " + lastTimestamp);
                    isCalledGetNewScans = true;
                    InstanceService.getDatabaseInstance().getNewScans(lastTimestamp);
                }
            }
        });
    }

    public void setNewScan(Scan scan) {
        newScan = scan;
    }

    public void calculateMyPosition() {
        Map<String, Integer> signals = newScan.getCombinedSignals();
        AlgType algType = AlgType.COMBINED;
        NearestNeighbor nn = cp.calculatePosition(signals, new ArrayList<>(scans), k, algType, true);
        if (!(nn.getX() == 0 && nn.getY() == 0)) {
            myPosition = nn;
            isCalculatedNewPosition = true;
        } else {
            isCalculatedNewPosition = false;
        }
        Log.d(C.TAG, "New signals are: " + signals);
    }

    // called from PlayerController.GetCalculatedPosition
    public NearestNeighbor getMyPosition() {
        Log.d(C.TAG, "My position is: " + myPosition);
        return myPosition;
    }

    public boolean isCalculatedNewPosition() {
        return isCalculatedNewPosition;
    }
}
