package cz.uhk.vojtele1.gameofflags.plugin.utils.scanners;

import cz.uhk.vojtele1.gameofflags.plugin.model.BleScan;
import cz.uhk.vojtele1.gameofflags.plugin.model.WifiScan;

import java.util.List;

/**
 * Created by Matej Danicek on 7.11.2015.
 * Reworked by Leon Vojtech on 2016-2019
 */
public interface ScanResultListener {

    void onScanFinished(List<WifiScan> wifiScans, List<BleScan> bleScans);
}
