package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.app.Activity;

public interface ErrorService {

    void setActivity(Activity activity);

    void showToast(String text);

    /**
     * Show message when loading takes too much time
     */
    void errorLoadingTooLong();
}
