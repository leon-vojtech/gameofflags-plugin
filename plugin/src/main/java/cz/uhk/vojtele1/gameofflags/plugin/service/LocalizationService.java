package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.support.v7.app.AppCompatActivity;

import cz.uhk.vojtele1.gameofflags.plugin.NN.NearestNeighbor;
import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;

public interface LocalizationService {

    void setActivity(AppCompatActivity activity);

    void setNewScan(Scan scan);

    void calculateMyPosition();

    NearestNeighbor getMyPosition();

    boolean isCalculatedNewPosition();
}
