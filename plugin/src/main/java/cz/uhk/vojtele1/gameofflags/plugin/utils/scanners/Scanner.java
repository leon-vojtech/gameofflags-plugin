package cz.uhk.vojtele1.gameofflags.plugin.utils.scanners;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.SystemClock;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.uhk.vojtele1.gameofflags.plugin.model.BleScan;
import cz.uhk.vojtele1.gameofflags.plugin.model.WifiScan;

/**
 * Created by Matej Danicek on 7.11.2015.
 * Reworked by Leon Vojtech on 2016-2019
 */
public class Scanner {

    private Activity activity;

    private List<WifiScan> wifiScans = new ArrayList<>();
    private List<BleScan> bleScans = new ArrayList<>();

    private long startTime;
    /**
     * zda prave probiha sken
     */
    private boolean running;

    private WifiManager wm;
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver wifiBroadcastReceiver;
    private Timer timer;
    private Handler handler;
    private BluetoothAdapter.LeScanCallback leScanCallback;

    private boolean isWifiBRRegistered, isBleBRRegistered;

    public Scanner(Activity activity) {
        this.activity = activity;
        init();
    }

    /**
     * Priprava broadcast receiveru, manazeru apod.
     */
    private void init() {
        wm = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //receiver pro prijem naskenovanych wifi siti
        wifiBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //System.out.println("Ziskal jsem wifiScany v case: " + (SystemClock.uptimeMillis() - startTime));
                wifiScans.addAll(convertWifiResults(wm.getScanResults()));
            }
        };
        leScanCallback = (device, rssi, scanRecord) -> {
            // System.out.println("Ziskal jsem ble rssi: " + rssi + " v case: " + (SystemClock.uptimeMillis() - startTime));
            bleScans.add(new BleScan(device.getAddress(), rssi, SystemClock.uptimeMillis() - startTime));
        };
    }

    private List<WifiScan> convertWifiResults(List<ScanResult> scanResults) {
        List<WifiScan> wifiScans = new ArrayList<>();
        for (ScanResult scan : scanResults) {
            WifiScan wifiScan = new WifiScan(scan.SSID, scan.BSSID, scan.level, SystemClock.uptimeMillis() - startTime, scan.frequency);
            wifiScans.add(wifiScan);
        }
        return wifiScans;
    }

    /**
     *
     * @param time is s
     * @param wifi
     * @param ble
     * @param scanResultListener
     * @return
     */
    public boolean startScan(int time, final boolean wifi, boolean ble, final ScanResultListener scanResultListener) {
        ble = ble && activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE); //vyradime ble pokud ho zarizeni nema.
        if (time == 0) time = 10;
        if (running || !enableHW(wifi, ble)) {
            return false; //pokud jeste nedobehlo probihajici skenovani (nebo problemy pri zapinani HW), NEstartuj nove a vrat false
        }
        running = true;

        wifiScans.clear();
        bleScans.clear();

        startTime = SystemClock.uptimeMillis(); //zaznamenej cas zacatku skenovani
        if (wifi) {
            //zaregistrovani receiveru pro wifi sken
            activity.registerReceiver(wifiBroadcastReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            isWifiBRRegistered = true;
            wm.startScan();
        }
        if (ble) {
            isBleBRRegistered = true;
            bluetoothAdapter.startLeScan(leScanCallback);
        }
        //casovac ukonceni skenovani
        timer = new Timer();
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        if (running && scanResultListener != null) {
                            scanResultListener.onScanFinished(wifiScans, new ArrayList<>(bleScans));
                        }
                        stopScan();
                    }
                }, time * 1000);

        // neda se zapnout novy scan az po obdrzeni vysledku (zacne to poustet spoustu scanu), proto jsou scany pousteny kazdou vterinu
        // jedna se jen o wifi (ble bezi neustale)
        handler = new Handler();
        int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                if (wifi) {
                    wm.startScan();
                }
                if (running) {
                    handler.postDelayed(this, delay);
                }
            }
        }, delay);
        return true;
    }

    public void stopScan() {
        if (!running) {
            return;
        }
        timer.cancel();

        if (isWifiBRRegistered) {
            try { // I got two times error (no more), so better catch not found receiver
                activity.unregisterReceiver(wifiBroadcastReceiver);
            } catch (Exception ignored) {}
        }
        if (isBleBRRegistered) {
            bluetoothAdapter.stopLeScan(leScanCallback);
        }

        isWifiBRRegistered = false;
        isBleBRRegistered = false;
        running = false;
        handler.removeCallbacks(() -> {});
    }

    /**
     * Zapne bt/wifi.
     *
     * @param wifi Jestli ma byt zapnuta wifi
     * @param ble  Jestli ma byt zapnut bt
     * @return Vraci true pokud jsou vsechny pozadovane adaptery zapnuty a pripraveny zacit skenovat
     */
    private boolean enableHW(boolean wifi, boolean ble) {
        if (ble) { //pokud nas zajima zapnuti BT
            final BluetoothAdapter btAdapter = ((BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
            if (btAdapter != null && !btAdapter.isEnabled()) {
                if (!btAdapter.enable()) {
                    Toast.makeText(activity, "Chyba při zapínání BT", Toast.LENGTH_SHORT).show();
                }
                return false; //zajima nas zapnuti BT ale ten je off -> navrat false protoze se musi pockat na jeho asynchronni zapnuti
            }
        }
        if (wifi) { //zajima nas zapnuti wifi
            if (wm.isWifiEnabled()) {
                return true; //wifi je zapla a ok
            } else {
                if (!wm.setWifiEnabled(true)) {
                    Toast.makeText(activity, "Chyba při zapínání WiFi", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        }
        return true; //chceme zapnout jen bt -> ktery je zaply protoze jsme dosli az sem
    }
}
