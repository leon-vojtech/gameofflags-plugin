package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.support.v7.app.AppCompatActivity;

import java.util.Map;

import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Fingerprint;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.OnlinePlayer;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Player;
import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Tower;

public interface DatabaseService {

    /**
     * Set activity for inserting scans to local db
     *
     * @param activity that is running
     */
    void setActivity(AppCompatActivity activity);

    /**
     * Listen for player changes in db, including initial player object at start
     */
    void addPlayerListener();

    /**
     * Remove listener for player changes
     */
    void removePlayerListener();

    /**
     * Called from PlayerController, could be null
     *
     * @return player object
     */
    Player getPlayer();

    /**
     * Set player nickname and fraction
     *
     * @param nickname - new nickname
     * @param fraction - new fraction number (1 or 2)
     */
    void setPlayer(String nickname, int fraction);

    /**
     * Set player object, used at Logout (set to null)
     *
     * @param player - object or null
     */
    void setPlayer(Player player);

    /**
     * Get user identifier, used for deciding if player is logged
     *
     * @return userUid or null
     */
    String getUserUid();

    /**
     * Add new fingerprint to database
     *
     * @param fingerprint - new fingerprint
     */
    void addFingerprint(Fingerprint fingerprint);

    /**
     * Add damage point
     */
    void addDamage();

    /**
     * Add repair point
     */
    void addRepair();

    /**
     * Get all flags at selected floor
     *
     * @param floor on that flags are located
     */
    Map<String, Tower> getTowersByFloor(String floor);

    /**
     * Get towers count for selected fraction
     *
     * @param fractionId of that towers will be counted
     * @return count of towers
     */
    int getTowersCountForFraction(int fractionId);

    /**
     * register all listeners
     */
    void registerListeners();

    /**
     * remove all listeners
     */
    void removeListeners();

    /**
     * Register towers listener
     */
    void addTowersListener();

    /**
     * Remove towers listener
     */
    void removeTowersListener();

    /**
     * @return true if database returned new data
     */
    boolean databaseActual();

    /**
     * change player fraction
     */
    void changeFraction();

    /**
     * save fingerprint to database with reference to screenshot in storage
     *
     * @param bytes of image
     * @param floor when tower is located
     * @param x     position of tower
     * @param y     position of tower
     */
    void saveFingerprintWithScreenshot(byte[] bytes, String floor, int x, int y);

    /**
     * add tower and player experience and change tower state (change fraction, HP)
     *
     * @param action    attack or repair
     * @param dmgRepair size of action
     * @param towerName of tower on that the action is executed
     */
    void saveAttackRepairTower(String action, int dmgRepair, String towerName);

    /**
     * set player to list with online players
     *
     * @param player that is set as online
     */
    void setPlayerOnline(Player player);

    /**
     * register player online listener
     */
    void addPlayerOnlineListener();

    /**
     * remove player online listener
     */
    void removePlayerOnlineListener();

    /**
     * get all online players for selected floor
     *
     * @param floor on that players will be shown
     * @return map of online players
     */
    Map<String, OnlinePlayer> getOnlinePlayersByFloor(String floor);

    /**
     * get all new scans for player localization
     *
     * @param lastScanTimestamp saved in database
     */
    void getNewScans(long lastScanTimestamp);
}
