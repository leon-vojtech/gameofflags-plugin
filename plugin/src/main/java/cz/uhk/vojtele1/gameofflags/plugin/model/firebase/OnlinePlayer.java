package cz.uhk.vojtele1.gameofflags.plugin.model.firebase;

public class OnlinePlayer {

    private String username;
    private int fraction;
    private int experience;
    private long lastChangedAt;
    private Position position;

    public OnlinePlayer() {
    }

    public OnlinePlayer(String username, int fraction, int experience, long lastChangedAt, Position position) {
        this.username = username;
        this.fraction = fraction;
        this.experience = experience;
        this.lastChangedAt = lastChangedAt;
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getFraction() {
        return fraction;
    }

    public void setFraction(int fraction) {
        this.fraction = fraction;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public long getLastChangedAt() {
        return lastChangedAt;
    }

    public void setLastChangedAt(long lastChangedAt) {
        this.lastChangedAt = lastChangedAt;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
