package cz.uhk.vojtele1.gameofflags.plugin.service.impl;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;

import java.util.Arrays;
import java.util.List;

import cz.uhk.vojtele1.gameofflags.plugin.service.AuthenticationService;
import cz.uhk.vojtele1.gameofflags.plugin.service.DatabaseService;
import cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService;

public class AuthenticationServiceImpl implements AuthenticationService {

    public static final int RC_SIGN_IN = 123;

    private static AuthenticationServiceImpl instance;

    private Activity activity;

    public static AuthenticationServiceImpl getInstance() {
        if (instance == null) {
            instance = new AuthenticationServiceImpl();
        }
        return instance;
    }

    private AuthenticationServiceImpl() {
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void createSignInIntent() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build());

        // Create and launch sign-in intent
        activity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    public void signOut() {
        AuthUI.getInstance()
                .signOut(activity)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        DatabaseService ds = InstanceService.getDatabaseInstance();
                        ds.removePlayerListener();
                        ds.setPlayer(null);
                    }
                });
    }
}
