package cz.uhk.vojtele1.gameofflags.plugin.utils;

public class C {

    private static final String UNITY_PACKAGE_NAME = "cz.uhk.vojtele1.gameofflags";
    public static final String SHARED_PREFERENCES = UNITY_PACKAGE_NAME + ".v2.playerprefs";

    public static final int DEFAULT_SCAN_TIME = 10; // in sec

    public static final String TAG = "plugin";

    public static final long WEEK_MILLISECONDS = 1000*60*60*24*7;

    public static final String PLAYER_FLOOR = "floor";
    public static final String DEFAULT_FLOOR = "J1NP";

    public static String AR_ATTACK = "attack";
    public static String AR_REPAIR = "repair";

    public static int TOWER_POPULARITY_INCREASE = 5;
    public static int ATTACK_REPAIR_EXPERIENCE_INCREASE = 10;

    public static final int ONLINE_PLAYERS_MAX_INACTIVE_TIME = 1000 * 60 * 2; // 2 minutes
}
