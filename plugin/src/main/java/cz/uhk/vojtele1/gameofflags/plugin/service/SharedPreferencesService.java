package cz.uhk.vojtele1.gameofflags.plugin.service;

import android.support.v7.app.AppCompatActivity;

/**
 * Shared preferences are created as service, because of using in class without activity.
 */
public interface SharedPreferencesService {

    void setActivity(AppCompatActivity activity);

    String getFloor();

    void setFloor(String floor);
}
