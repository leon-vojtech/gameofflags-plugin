package cz.uhk.vojtele1.gameofflags.plugin.service.impl;

import android.app.Activity;
import android.util.Log;

import cz.uhk.vojtele1.gameofflags.plugin.model.firebase.Fingerprint;
import cz.uhk.vojtele1.gameofflags.plugin.service.CaptureService;
import cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService;
import cz.uhk.vojtele1.gameofflags.plugin.utils.C;
import cz.uhk.vojtele1.gameofflags.plugin.utils.scanners.DeviceInformation;
import cz.uhk.vojtele1.gameofflags.plugin.utils.scanners.Scanner;
import cz.uhk.vojtele1.gameofflags.plugin.utils.scanners.SensorScanner;

public class CaptureServiceImpl implements CaptureService {

    private static CaptureServiceImpl instance;

    private Scanner scanner;
    private SensorScanner sensorScanner;
    private DeviceInformation deviceInformation;
    private Fingerprint fingerprint;

    public static CaptureServiceImpl getInstance() {
        if (instance == null) {
            instance = new CaptureServiceImpl();
        }
        return instance;
    }

    private CaptureServiceImpl() {
    }

    public void setActivity(Activity activity) {
        scanner = new Scanner(activity);
        sensorScanner = new SensorScanner(activity);
        deviceInformation = new DeviceInformation(activity);
    }

    public void createFingerprint() {
        Log.d(C.TAG, "start createFingerprint");
        sensorScanner.addListeners();
        scanner.stopScan();
        scanner.startScan(C.DEFAULT_SCAN_TIME, true, true, (wifiScans, bleScans) -> {
            // odregistruj listener
            sensorScanner.removeListener();
            Log.d(C.TAG, "Scan finished, filling data");
            Fingerprint f = new Fingerprint();
            f.setWifiScans(wifiScans);
            f.setBleScans(bleScans);
            sensorScanner.fillPosition(f); // naplnime daty ze senzoru -- musi se definovat predem, jinak to nestihne data zapsat
            deviceInformation.fillPosition(f); // naplnime infomacemi o zarizeni
            f.setTimestamp(System.currentTimeMillis());

            fingerprint = f;
        });
    }

    public Fingerprint getFingerprint() {
        return fingerprint;
    }

    public void attackRepairAction(String action, int dmgRepair, String towerName) {
        InstanceService.getDatabaseInstance().saveAttackRepairTower(action, dmgRepair, towerName);
    }
}
