package cz.uhk.vojtele1.gameofflags.plugin;

import android.Manifest;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.firebase.ui.auth.IdpResponse;
import cz.uhk.vojtele1.gameofflags.plugin.model.Scan;
import cz.uhk.vojtele1.gameofflags.plugin.service.AuthenticationService;
import cz.uhk.vojtele1.gameofflags.plugin.service.DatabaseService;
import cz.uhk.vojtele1.gameofflags.plugin.service.ErrorService;
import cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService;
import cz.uhk.vojtele1.gameofflags.plugin.service.LocalizationService;
import cz.uhk.vojtele1.gameofflags.plugin.service.impl.AuthenticationServiceImpl;
import cz.uhk.vojtele1.gameofflags.plugin.utils.C;
import cz.uhk.vojtele1.gameofflags.plugin.utils.scanners.Scanner;
import cz.uhk.vojtele1.gameofflags.plugin.utils.UnityPlayerActivity;
import cz.uhk.vojtele1.gameofflags.plugin.utils.Utils;

public class OverrideUnityActivity extends UnityPlayerActivity {
    private static final int REQUEST_PERMISSION_CODE = 1;

    public void onBackPressed()
    {
        // instead of calling UnityPlayerActivity.onBackPressed() we just ignore the back button event
        //super.onBackPressed();
    }

    private boolean wasBTEnabled, wasWifiEnabled;
    private WifiManager wm;
    private BluetoothAdapter bluetoothAdapter;
    private Scanner scanner;

    private boolean running = false;

    private DatabaseService databaseService;
    private LocalizationService localizationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(C.TAG, "Localization - OverrideUnityActivity onCreate");
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        scanner = new Scanner(this);

        // pass activity to services, that need it
        InstanceService.createServicesInstances(this);
        databaseService = InstanceService.getDatabaseInstance();
        localizationService = InstanceService.getLocalizationInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermissions();
        Log.d(C.TAG, "Localization - OverrideUnityActivity onResume");
        wasBTEnabled = bluetoothAdapter.isEnabled();
        wasWifiEnabled = wm.isWifiEnabled();
        Utils.changeBTWifiState(true, wm, bluetoothAdapter, wasWifiEnabled, wasBTEnabled);
        running = true;
        scanRepeater();
        databaseService.registerListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(C.TAG, "Localization - OverrideUnityActivity onPause");
        Utils.changeBTWifiState(false, wm, bluetoothAdapter, wasWifiEnabled, wasBTEnabled);
        running = false;
        databaseService.removeListeners();
        scanner.stopScan();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                // we don't have permissions, quit the app
                this.finishAffinity();
            }
        }
    }

    private void checkPermissions() {
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.BLUETOOTH,
                            Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PERMISSION_CODE);
        }
    }

    private void doScan() {
        Log.d(C.TAG, "Localization - OverrideUnityActivity doScan");
        // we want to scan all (wifi and ble)
        scanner.startScan(C.DEFAULT_SCAN_TIME, true, true, (wifiScans, bleScans) -> {
            // x, y and level are not important here, we need just scans
            localizationService.setNewScan(new Scan(wifiScans, bleScans, 0, 0, "", System.currentTimeMillis()));
            localizationService.calculateMyPosition();
            if (localizationService.isCalculatedNewPosition()) {
                databaseService.setPlayerOnline(databaseService.getPlayer());
            }
        });
    }

    /**
     * doScan is called only once, if it waits for listener, so repeater is needed
     */
    private void scanRepeater() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                doScan();
                if (running) {
                    handler.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AuthenticationServiceImpl.RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                databaseService.addPlayerListener();
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

}
