package cz.uhk.vojtele1.gameofflags.plugin.utils;

import android.bluetooth.BluetoothAdapter;
import android.net.wifi.WifiManager;

import java.util.*;

public class Utils {

    /**
     * Zapne BT a Wifi pokud je aktivita aktivni. Pokud bylo BT nebo Wifi zaple, zustane zaple.
     *
     * @param enable jestli se ma bt a wifi zapnout/vypnout
     * @return true
     */
    public static boolean changeBTWifiState(boolean enable, WifiManager wm, BluetoothAdapter bluetoothAdapter, boolean wasWifiEnabled, boolean wasBTEnabled) {
        if (enable) {
            if (!wasBTEnabled && !wasWifiEnabled) {
                wm.setWifiEnabled(true);
                return bluetoothAdapter.enable();
            } else if (!wasBTEnabled) {
                return bluetoothAdapter.enable();
            } else
                return wasWifiEnabled || wm.setWifiEnabled(true);
        } else {
            if (!wasBTEnabled && !wasWifiEnabled) {
                wm.setWifiEnabled(false);
                return bluetoothAdapter.disable();
            } else if (!wasBTEnabled) {
                return bluetoothAdapter.disable();
            } else
                return wasWifiEnabled || wm.setWifiEnabled(false);
        }
    }
    // TODO stejný metody jsou v c#, možná sjednotit
    /**
     * Calculate tower max hp
     * @param level of tower
     * @return max hp
     */
    public static int calculateTowerMaxHealthPoints(int level)
    {
        return 20 * level;
    }

    /**
     * It increases by 40 per level, so 20, 80, 180, 320, 500, ... first level needs 20 exp, second 60 etc.
     */
    private static final double levelConst = 0.22360679775;

    public static int calculateLevel(int experience)
    {
        return (int)Math.floor(levelConst * Math.sqrt(experience)) + 1;
    }
}
